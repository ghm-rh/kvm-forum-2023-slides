# targets
all: talk.pp.pdf
handout: talk.handout.pdf
notes: talk.notes.pdf
talk.pp.tex: talk-hanna.tex talk-german.tex 
# talk.text.pdf -- don't build article version by default

.PRECIOUS: talk.pp.pdf
.PHONY: show

TEXINPUTS=.:$(RHBEAMER):
export TEXINPUTS

# path to rh beamer
RHBEAMER=rh-beamer-theme

# common tex makefile
include $(RHBEAMER)/Makefile.common

# disable auto opening of pdf since the "show" target open it
show: SHOW = NO
show: talk.pp.pdf
	$(PDFVIEWER) $^
