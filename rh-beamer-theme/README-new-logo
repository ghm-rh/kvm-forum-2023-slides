The following four files in this directory:

    RHCLogoHRE.pdf
    RHCLogoHRE.eps
    rhbg.png
    rhbg.pdf
    
are supposed to be drop-in replacement for the older Red Hat logo and
"Encaptulated Postscript" (.eps) file that are part of the
'rh-beamker-theme' repo from Karel Zak:

    git://git.engineering.redhat.com/users/kzak/rh-beamer-theme.git

    - - -

Thanks: Máirín Duffy <duffy@redhatr.com> for preparing the
'rhbg-blank.png' and 'rhbg-newlogo.png' files on this 'tech-list'
thread:
<http://post-office.corp.redhat.com/archives/tech-list/2019-May/msg00109.html>

    - - -


NOTES
-----

(1) The 'rhbg.png' file is downloaded from here (and renamed):
    https://pnt.redhat.com/pnt/p-11629415/Logo-RedHat-A-Color-RGB.png

(2) I generated the 'RHCLogoHRE.eps' and 'RHCLogoHRE.pdf' files by
    simply running:

      $> convert rhbg.png RHCLogoHRE.eps
      $> convert rhbg.png RHCLogoHRE.pdf


Known Issues
------------

If you're using 16:9 aspect ratio with Beamer+LaTeX, then the logo in
the main title slides and "part" slides gets distorted, but for the
regular 4:3 slides, the logo stays intact.

Compare the logo on the top-left with 16:9 & 4:3 aspect ratios:

 - 16:9: http://file.emea.redhat.com/~kchamart/Distorted-new-logo-16-9.png 
 -  4:3: http://file.emea.redhat.com/~kchamart/Undistorted-new-logo-4-3.png

However, for the rest of the content slides, which are the majority, the
new Red Hat logo stays undistorted and normally.  


(Unsatisfying) "Workaround"
--------------------------

I replaced (i.e. overwrote) the 'rhbg.png' file with the
'templates/rhbg-blank.png'.  This means, the main slide and the "part"
slides will get no Red Hat logo at all, instead of a distorted hat plus
logo, which is undesirable.
