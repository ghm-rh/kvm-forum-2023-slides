\begin{frame}{Content}
    \begin{enumerate}
        \item virtio-fs and virtiofsd today (overview, options)
        \item Live migration
        \item Integration with other projects
        \item virtio-fs from a technical perspective
        \item Future plans
    \end{enumerate}
\end{frame}

\begin{frame}{Overview}{}
    {\bf virtio-fs:} sharing a directory tree between host and VMs \\
    {\bf virtiofsd:} vhost-user device daemon written in Rust

    \begin{center}
        \includegraphics[scale=0.4]{imgs/virtiofsd.eps}
    \end{center}

    \vfill

    \begingroup
        \fontsize{10pt}{12pt}\selectfont
        \href{https://virtio-fs.gitlab.io/}{https://virtio-fs.gitlab.io/} \\
	\href{https://gitlab.com/virtio-fs/virtiofsd}{https://gitlab.com/virtio-fs/virtiofsd}
    \endgroup
\end{frame}

\section{Options You Should Know About}

% Options skipped here:
% -- killpriv v2 [S -- too complicated to explain, probably too niche]
% -- thread pool [S]

% NOTE: Decided to skip this.  The default works nicely for most people, and if you need something
% else, you should be able to figure this out, because the options are obvious enough.
% Maybe just talk about --sandbox=namespace in more detail, including the UID/GID map?
% Together with German decided not to talk about that either.  Nobody cares, it’s boring.
%\begin{frame}{virtiofsd Options: Sandbox Modes}
%    % Are there planned changes for this, e.g. external sandboxing?
%    virtiofsd can sandbox itself:
%    \begin{itemize}
%        \item {\tt namespace} (default): \\
%            separate mount, PID, network, user (unless root) namespaces; \\
%            {\tt pivot\_root} to the shared directory
%        \item {\tt chroot} (requires root): \\
%            {\tt chroot} into the shared directory
%        \item {\tt none}
%    \end{itemize}
%\end{frame}

\begin{frame}{Cache Modes}
    % TODO: Make a note what assumptions must be fulfilled in each mode!

    Tell guest how to cache:
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item {\tt --cache=never}:
                    \begin{itemize}
                        \item Disable page and dentry caches
                    \end{itemize}
                \item {\tt --cache=auto}:
                    \begin{itemize}
                        \item Use defaults (read cache, writethrough)
                        \item Cache dentries for 1~s
                    \end{itemize}
                \item {\tt --cache=always}:
                    \begin{itemize}
                        \item Keep page cache when (re-)opened
                        \item Cache whole directories (indefinitely)
                    \end{itemize}
                \item {\tt --writeback}:
                    \begin{itemize}
                        \item Writeback cache
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{center}
                \includegraphics[scale=0.55]{imgs/cache-safety.eps}
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Context for {\tt --inode-file-handles}}
    \begin{center}
        \includegraphics[scale=0.6]{imgs/relative-addressing.eps}
    \end{center}

    \vspace{5mm}
    \begin{center}
        $\to$ virtiofsd must open inodes from ID
    \end{center}
\end{frame}

\begin{frame}{{\tt O\_PATH} vs. File Handles}
    Mapping inode ID $\to$ inode:
    \begin{enumerate}
        \item {\tt O\_PATH} FDs
            \begin{itemize}
                \item FD count limit
                \item Keeps files open: Problems with unlinking (NFS silly rename)
                    % TODO: Need details on who is deleting
            \end{itemize}
        \item File handles: Pure data, per-FS unique ID locating an inode
            \begin{itemize}
                \item Needs {\tt CAP\_DAC\_READ\_SEARCH} (i.e., root), FS support
            \end{itemize}
    \end{enumerate}

    \vspace{5mm}
    \begin{center}
        \includegraphics[scale=0.5]{imgs/file-handle.eps}
    \end{center}
\end{frame}

\begin{frame}{Configuration}{}
    {\tt --inode-file-handles}:
    \begin{itemize}
        \item {\tt never} (default): Always use {\tt O\_PATH FDs}
        \item {\tt prefer}: Use file handles if supported
        \item {\tt mandatory}: Never use {\tt O\_PATH FDs}
    \end{itemize}
\end{frame}

\section{Live Migration}

\begin{frame}{Problem: Internal State}
    \begin{columns}
        \begin{column}{0.7\textwidth}
            State:
            \begin{itemize}
                \item Mappings of inode IDs to inodes
                \item FDs for open files
            \end{itemize}

            \vspace{5mm}

            $\to$ Transfer and restore on destination

            \vspace{5mm}

            Problems:
            \begin{enumerate}
                \item Serializing/deserializing
                \item Transfer channel
            \end{enumerate}
        \end{column}
        \begin{column}{0.3\textwidth}
            \vspace{-23mm}
            \begin{center}
                \includegraphics[scale=0.4]{imgs/internal-state-transfer.eps}
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Transfer Channel:\\QEMU’s Migration Stream}
    \begin{columns}
        \begin{column}{0.7\textwidth}
            \begin{itemize}
                \item No additional configuration/privileges
                \item Make use of QEMU’s features \\
                    (e.g. migration through file)
                \item Requires extending the vhost(-user) protocol
                    \begin{itemize}
                        \item Blocking blob transfer during downtime
                        \item Proposal/discussion: \href{https://lists.nongnu.org/archive/html/qemu-devel/2023-04/msg01575.html}{https://lists.nongnu.org/archive/html/qemu-devel/2023-04/msg01575.html}
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.3\textwidth}
            \vspace{-29.3mm}
            \begin{center}
                \includegraphics[scale=0.4]{imgs/internal-state-transfer-through-qemu.eps}
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Serialization: Indexed Inodes}
    \begin{columns}
        \begin{column}{0.7\textwidth}
            File handles:
            \begin{itemize}
                \item Either immediately, or by converting FDs
                \item Requires {\tt CAP\_DAC\_READ\_SEARCH}
                \item Only valid on the very same FS
            \end{itemize}

            \vspace{5mm}
            File paths:
            \begin{itemize}
                \item Need to be reconstructed (costly)
                \item Allows migration to different FS
                \item Cannot handle external renames
            \end{itemize}
        \end{column}
        \begin{column}{0.3\textwidth}
            \vspace{-20mm}
            \begin{center}
                \includegraphics[scale=0.5]{imgs/file-to-data.eps}
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Serialization: Open Files}
    \begin{itemize}
        \item Transfer associated inode ID + {\tt openat()} flags $\to$ reopen
            \begin{itemize}
                \item Problem: File deleted
                \item Problem: Permissions changed
            \end{itemize}
        \item Silly renaming?  Delaying {\tt unlink}?  Tricky.
            % It is tradition that virtio-fs keeps hitting the exact same problems as NFS.
        \item Cannot transfer FDs without same-time same-host same-FS channel
            \begin{itemize}
                \item Add same-virtiofsd-process restriction: Skip state transfer altogether
                    \begin{itemize}
                        \item See “external migration” work from Anton Kuchin \\
                            \href{https://lists.nongnu.org/archive/html/qemu-devel/2023-02/msg05295.html}{https://lists.nongnu.org/archive/html/qemu-devel/2023-02/msg05295.html}
                    \end{itemize}
            \end{itemize}
    \end{itemize}
\end{frame}

%\begin{frame}{Road Blocks}
%    \begin{itemize}
%        \item Details around vhost-user extensions
%        \item Bugs in virtiofsd’s vhost-user implementation
%        \item Dirty bitmaps for memory write tracking during migration
%        \item Exact implementation of path reconstruction
%        \item Unknown unknowns
%    \end{itemize}
%\end{frame}
